# B11.5 Fluend

## Задание B11.5.1

Используя *Fluentd*, необходимо создать фильтр, который будет отбрасывать полученные данные, если они содержат "*localhost*" в поле "*source*". Может потребоваться фильтр [*grep*](https://docs.fluentd.org/filter/grep) и плагин вывода [*null*](https://docs.fluentd.org/output/null).

Тренироваться можно на сообщении 
```
‘json={“source”: “localhost”, “message”: “test”}’
```

## Решение

Файл [td-agent.conf](https://gitlab.com/MaXX74/b11-practice/-/blob/main/b11.5.1/td-agent.conf)

```
<source>
  @type http
  port 8888
</source>

<filter test1>
  @type grep
  <exclude>
    key source
    pattern /localhost/
  </exclude>
</filter>

<match test1>
  @type file
  path /var/log/fluent/access
</match>
```
## Результат

![Image](https://gitlab.com/MaXX74/b11-practice/-/raw/main/b11.5.1/pics/result.png)
