# Модуль B11 - Журналирование

## Содержание

- [Задание B11.5.1](https://gitlab.com/MaXX74/b11-practice/-/tree/main/b11.5.1) - Fluentd
- [Задание B11.6](https://gitlab.com/MaXX74/b11-practice/-/tree/main/b11.6) - ELK - Elastic Stack (ElasticSearch, Logstash, Kibana)
- [Практикум B11.9](https://gitlab.com/MaXX74/b11-practice/-/tree/main/b11.9) - Итоговое заданеи модуля